import { put, takeEvery } from "redux-saga/effects";

import { api } from "~/store";
import { IHouse } from "~/types/IHouse";
import { handleError } from "~/services/utils";
import { setLoaded, setLoading } from "../common";
import { setHouseListAction, HouseListActions } from "./houses";

export function* sagaGetHouseList() {
  const actionType = HouseListActions.GET_HOUSES;

  try {
    yield put(setLoading({ actionType }));

    const res: IHouse[] = yield api.questionsApi.getHouseList();

    yield put(setHouseListAction(res));
    yield put(setLoaded({ actionType }));
  } catch (error) {
    const message = error.message || "GET HOUSES ERROR";

    console.error(message);
    handleError({
      actionType,
      message,
    });
  }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default function* (): Generator {
  yield takeEvery(HouseListActions.GET_HOUSES, sagaGetHouseList);
}
