import { Reducer } from "redux";

import { IHouse } from "~/types/IHouse";

// Actions
export enum HouseListActions {
  GET_HOUSES = "questions/GET_HOUSES",
  SET_HOUSES = "questions/SET_HOUSES",
  RESET = "questions/RESET",
}

export type HouseListLoadableT = typeof HouseListActions.GET_HOUSES;

export interface IGetHouseListAction {
  type: typeof HouseListActions.GET_HOUSES;
}

export interface ISetHouseListAction {
  type: typeof HouseListActions.SET_HOUSES;
  payload: IHouse[];
}

export interface IResetHouseListAction {
  type: typeof HouseListActions.RESET;
}

type HouseListActionsT =
  | IGetHouseListAction
  | ISetHouseListAction
  | IResetHouseListAction;
export const getHouseListAction = (): IGetHouseListAction => ({
  type: HouseListActions.GET_HOUSES,
});

export const setHouseListAction = (payload: IHouse[]): ISetHouseListAction => ({
  type: HouseListActions.SET_HOUSES,
  payload,
});

export const resetHouseListAction = (): IResetHouseListAction => ({
  type: HouseListActions.RESET,
});

// Reducer
export interface IHouseListState {
  list: IHouse[];
}

export type HouseListStateT = Readonly<IHouseListState>;

const initialState: IHouseListState = {
  list: [],
};
const reducer: Reducer<HouseListStateT> = (
  state: IHouseListState = initialState,
  action: HouseListActionsT
) => {
  switch (action.type) {
    case HouseListActions.SET_HOUSES:
      return {
        ...state,
        list: action.payload,
      };

    case HouseListActions.RESET:
      return initialState;

    default:
      return state;
  }
};

export default reducer;
