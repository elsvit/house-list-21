import BaseApi from './BaseApi';
import HouseListApi from './HouseListApi';

export interface IApiServices {
  baseApi: BaseApi,
  questionsApi: HouseListApi,
}

export function initApiServices(baseUrl?: string): IApiServices {
  const baseApi = new BaseApi(baseUrl);
  const questionsApi = new HouseListApi(baseApi);

  return {
    baseApi,
    questionsApi
  };
}