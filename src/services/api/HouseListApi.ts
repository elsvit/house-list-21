import {IHouse} from '~/types/IHouse';
import BaseApi from './BaseApi';

const urlGetHouses = '/huvber/b51c0279d3f452513a7c1f576a54f4d7/raw/4497a12b181713c6856303a666d240f7d561e4fe/mock-house';

export default class HouseListApi {
  constructor(baseApi: BaseApi) {
    this.baseApi = baseApi;
  }

  public baseApi: BaseApi;
  public getHouseList = (): Promise<IHouse[]> => this.baseApi.get(urlGetHouses);
}

export type HouseListResponseTypes = IHouse[] & IHouse & void