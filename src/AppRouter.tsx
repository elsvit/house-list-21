/**
 * @fileOverview Routers
 */

import {ConnectedRouter} from 'connected-react-router';
import * as React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import {history} from './store';
import {HouseListPage} from './components/pages';
import DrawerWrapper from './components/blocks/DrawerWrapper';

import {ROUTES} from './constants';

const AppRouter = () => {
  return (
    <ConnectedRouter history={history}>
      <DrawerWrapper>
        <Switch>
          <Route exact path={ROUTES.LIST} component={HouseListPage} />
          <Redirect to={ROUTES.LIST} />
        </Switch>
      </DrawerWrapper>
      <NotificationContainer/>
    </ConnectedRouter>
  );
};

export default AppRouter;