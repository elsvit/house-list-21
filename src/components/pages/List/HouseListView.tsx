import React, { ReactElement } from "react";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

import { IHouse } from "~/types/IHouse";
import HouseListCard from "./HouseListCard";
import "./houseList.scss";

interface IRegionsViewProps {
  list: IHouse[];
  onClick: (id: string) => void;
}

const HouseListView = ({ list, onClick }: IRegionsViewProps): ReactElement => {
  return (
    <div className="houses-wrapper">
      <div className="houses-header">
        <Typography variant="h5" gutterBottom>
          {"HOUSES"}
        </Typography>
      </div>
      <Divider className="divider" variant="fullWidth" />
      {list.map((data: IHouse) => (
        <HouseListCard key={data.id} data={data} onClick={onClick} />
      ))}
    </div>
  );
};

export default HouseListView;
