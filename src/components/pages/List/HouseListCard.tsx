import React from "react";
import { IHouse } from "~/types/IHouse";
import { Label, LabelType, Button, BookedLabel } from "~/components/ui";
import { ImagePlaceholderIcon } from "~/assets/icons";

interface IHouseListCard {
  data: IHouse;
  onClick: (id: string) => void;
}

const HouseListCard = ({ data, onClick }: IHouseListCard) => {
  const { id, name, image, bookable, booked } = data;

  const labelType = bookable
    ? booked
      ? LabelType.booked
      : LabelType.available
    : LabelType.unavailable;

  const handleClick = () => {
    if (bookable) {
      onClick(id);
    }
  };
  const btnText = bookable ? "Book" : "Not Bookable";

  return (
    <div className="house-list-card">
      <div className="top-section">
        {image ? (
          <div
            className="top-section-bg"
            style={{ backgroundImage: `url(${image})` }}
          />
        ) : (
          <div className="top-section-placeholder">
            <ImagePlaceholderIcon width={46} height={36.8} />
          </div>
        )}
        <Label type={labelType} className="top-section-label" />
      </div>
      <div className="bottom-section">
        <div className="id-text">{`Id: ${id}`}</div>
        <div className="name-text">{name}</div>
        <div className="btn-wrapper">
          {booked ? (
            <BookedLabel bookedDays={booked} className="book-label"/>
          ) : (
            <Button
              text={btnText}
              isDisabled={!bookable}
              onClick={handleClick}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default HouseListCard;
