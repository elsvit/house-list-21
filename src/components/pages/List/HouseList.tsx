import { push, RouterAction } from "connected-react-router";
import React, { useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";

import HouseListView from "./HouseListView";
import { IAppState } from "~/store";
import { IHouse } from "~/types/IHouse";
import { getHouseListAction } from "~/store/houses";

interface IStateMap {
  list: IHouse[];
  loading?: IBoolDict;
}

interface IDispatchMap {
  getHouseList: typeof getHouseListAction;
  navTo: (route: string) => RouterAction;
}

type IHouseListProps = IStateMap & IDispatchMap;

const HouseList = ({ list, getHouseList, navTo }: IHouseListProps) => {
  useEffect(() => {
    getHouseList();
  }, [getHouseList]);

  const onCardClick = (id: string) => {
    // TODO
    console.log("id:", id);
  };

  return <HouseListView list={list} onClick={onCardClick} />;
};

const mapStateToProps = ({
  questions: { list },
  common: { loading },
}: IAppState) => ({
  list,
  loading,
});
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      getHouseList: getHouseListAction,
      navTo: (route: string) => push(route),
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(HouseList);
