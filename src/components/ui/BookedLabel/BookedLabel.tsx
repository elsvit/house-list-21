import cn from "classnames";
import React from "react";

import { TicIcon } from "~/assets/icons";
import "./bookedLabel.scss";

interface ILabelProps {
  bookedDays: number;
  className?: string;
}

const BookedLabel = ({ className, bookedDays = 0 }: ILabelProps) => {
  const text = `Booked for ${bookedDays} day${bookedDays > 1 ? "s" : ""}`;
  return (
    <div className={cn("booked-label", className)}>
      <TicIcon width={16} height={16} />
      <div className="booked-label-text">{text}</div>
    </div>
  );
};

export default BookedLabel;
