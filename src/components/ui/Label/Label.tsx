import cn from 'classnames';
import React, {ReactElement} from 'react';

import './label.scss';

export enum LabelType {
  available = 'available',
  booked = 'booked',
  unavailable = 'unavailable',
}

interface ILabelProps {
  type: LabelType
  className?: string;
}

const Label = ({className, type = LabelType.available}: ILabelProps) => {
  let colorClass;
  switch (type) {
    case LabelType.unavailable:
      colorClass = 'label-red';
      break;
    case LabelType.booked:
      colorClass = 'label-blue';
      break;
    default:
      colorClass = 'label-green';
      break;
  }

  return (
    <div className={cn('label', colorClass, className)}>
      {type}
    </div>
  );
};

export default Label;