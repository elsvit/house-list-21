import cn from "classnames";
import React, { ReactElement } from "react";

import "./button.scss";

export enum CustomButtonColor {
  red = "red",
  green = "green",
  blue = "blue",
}

interface ICustomButtonProps {
  text: string;
  onClick: () => void;
  isDisabled?: boolean;
  className?: string;
}

const Button = ({
  text,
  onClick,
  isDisabled,
  className,
}: ICustomButtonProps): ReactElement => {
  return (
    <div
      className={cn("custom-button", { disabled: isDisabled }, className)}
      onClick={onClick}
    >
      {text}
    </div>
  );
};

export default Button;
