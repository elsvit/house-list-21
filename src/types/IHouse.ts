export interface IHouse {
  id: string;
  name: string;
  image: string;
  bookable: boolean;
  booked: number;
}
